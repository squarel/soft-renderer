var srender = (function (srender) {
    srender.Vec3 = BABYLON.Vector3;

    var Mock = {};
    Mock.Cube = function (x, y, z) {
        var mesh = new srender.Mesh(8);
        mesh.vertices[0] = new srender.Vec3(-x, y, -z);
        mesh.vertices[1] = new srender.Vec3(x, y, -z);
        mesh.vertices[2] = new srender.Vec3(x, y, z);
        mesh.vertices[3] = new srender.Vec3(-x, y, z);
        mesh.vertices[4] = new srender.Vec3(x, -y, -z);
        mesh.vertices[5] = new srender.Vec3(x, -y, z);
        mesh.vertices[6] = new srender.Vec3(-x, -y, z);
        mesh.vertices[7] = new srender.Vec3(-x, -y, -z);

        // TODO: use triangle strip
        mesh.faces[0] = new srender.Face(0, 1, 2);
        mesh.faces[1] = new srender.Face(0, 2, 3);
        mesh.faces[2] = new srender.Face(1, 2, 5);
        mesh.faces[3] = new srender.Face(1, 4, 5);
        mesh.faces[4] = new srender.Face(0, 3, 6);
        mesh.faces[5] = new srender.Face(0, 6, 7);
        mesh.faces[6] = new srender.Face(4, 5, 7);
        mesh.faces[7] = new srender.Face(5, 6, 7);
        mesh.faces[8] = new srender.Face(2, 3, 5);
        mesh.faces[9] = new srender.Face(3, 5, 6);
        mesh.faces[10] = new srender.Face(0, 1, 4);
        mesh.faces[11] = new srender.Face(0, 4, 7);
        return mesh;
    };

    srender.Mock = Mock;
    return srender;
}(srender || {}));
