var canvas, model, camera, init;
document.addEventListener("DOMContentLoaded", init, false);

// init all the objects we need, gets called after DOM loading finish
init = function() {
    console.log("start");
    canvas = document.getElementById("myCanvas");
    camera = new srender.Camera();
    camera.pos = new srender.Vec3(0, 0, 10);
    camera.target = new srender.Vec3(0, 0, 0);
    model = srender.Mock.Cube(0.5, 0.5, 0.5);
    shader = new srender.Shader(camera, model, canvas);

    requestAnimationFrame(renderLoop);
}

renderLoop = function() {
    model.rot.x += 0.01;
    model.rot.y += 0.01;
    //camera.pos.z += 0.01;

    shader.clearBuffer();
    shader.render();
    shader.bufferFlip();
    requestAnimationFrame(renderLoop);
}

