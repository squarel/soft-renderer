var srender = (function (srender) {
    srender.Vec3 = BABYLON.Vector3;
    srender.Matrix = BABYLON.Matrix;
    srender.Color = BABYLON.Color4;
    srender.Vec2 = BABYLON.Vector2;

    srender.Shader = (function () {
        function Shader(camera, model, canvas) {
            this.camera = camera;
            this.model = model;
            this.canvas = canvas;
            this.context = canvas.getContext("2d");
        }

        var _createModelMatrix = function (model) {
            var rotation_matrix, translation_matrix;

            rotation_matrix = srender.Matrix.RotationYawPitchRoll(
                model.rot.y, model.rot.x, model.rot.z);
                translation_matrix = srender.Matrix.Translation(
                model.pos.x, model.pos.y, model.pos.z);

            // golden rule: first rotate, then translate
            return rotation_matrix.multiply(translation_matrix);

        };

        var _createViewMatrix = function (camera) {
            var eye = camera.pos;
            var target = camera.target;
            var up = srender.Vec3.Up();
            return srender.Matrix.LookAtLH(eye, target, up);
        };

        var _offset = function(value, metric) {
            return value * metric + metric / 2.0;
        }

        var _createProjectionMatrix = function (canvas) {
            var fov = 0.5;
            var ratio_aspect = canvas.width / canvas.height;
            var near = 0.01;
            var far = 1.0;
            return srender.Matrix.PerspectiveFovLH(fov, ratio_aspect, near, far);

        };

        var _project = function (vert_index, transform_matrix) {
                var point = srender.Vec3.TransformCoordinates(
                    this.model.vertices[vert_index], transform_matrix);
                // canvas starts from left top, should add that offset
                point.x = _offset(point.x, canvas.width);
                point.y = _offset(-point.y, canvas.height);
                return point;
        }

        // image data is a 1-D array contains RGBA for each index
        var _getIndexFromImageData = function (x, y) {
            return ((y >> 0) * this.canvas.width + (x >> 0)) * 4;
        }

        Shader.prototype.clearBuffer = function () {
            this.context.clearRect(0, 0, canvas.width, canvas.height);
            this.frameBuffer = this.context.getImageData(
                0, 0, canvas.width, canvas.height);
        };

        Shader.prototype.bufferFlip = function () {
            this.context.putImageData(this.frameBuffer, 0, 0);
        };

        Shader.prototype.drawPoint = function (point, color) {
            this.frameBufferData = this.frameBuffer.data;
            var index = _getIndexFromImageData(point.x, point.y);
            this.frameBufferData[index] = color.r * 255;
            this.frameBufferData[index + 1] = color.g * 255;
            this.frameBufferData[index + 2] = color.b * 255;
            this.frameBufferData[index + 3] = color.a * 255;
        };

        Shader.prototype.drawLine = function(point_a, point_b, color) {
            var dist = point_b.subtract(point_a).length();
            if (dist < 2)
                return;

            var middle_point = point_a.add(point_b.subtract(point_a).scale(0.5));
            this.drawPoint(middle_point, color);
            this.drawLine(point_a, middle_point, color);
            this.drawLine(middle_point, point_b, color);
        };

        Shader.prototype.drawBresenhamLine = function(point_a, point_b, color) {
            var x0 = point_a.x >> 0;
            var x1 = point_b.x >> 0;
            var y0 = point_a.y >> 0;
            var y1 = point_b.y >> 0;
            var dx = Math.abs(x1 - x0);
            var dy = Math.abs(y1 - y0);
            var sx = (x0 < x1) ? 1 : -1;
            var sy = (y0 < y1) ? 1 : -1;
            var err = dx - dy;
            var err2;

            while(1) {
                var point = new srender.Vec3(x0, y0, 0);
                this.drawPoint(point, color);
                if(x0 == x1 && y0 == y1)
                    break;
                err2 =  err * 2;
                if(err2 > -dy) {
                    err -= dy;
                    x0 += sx;
                }
                if(err2 < dx) {
                    err += dx;
                    y0 += sy;
                }
            }
        };

        Shader.prototype.render = function () {
            var model_matrix = _createModelMatrix(this.model);
            var view_matrix = _createViewMatrix(this.camera);
            var projection_matrix = _createProjectionMatrix(this.canvas)

            // order is very very important
            var transform_matrix = model_matrix.multiply(view_matrix).multiply(projection_matrix);

            // transform each vertices then draw on the frame buffer
            var prev_point = -1;

            this.model.faces.forEach(function(face) {
                var c_white = new srender.Color(1, 1, 1 , 1);

                var v1 = _project(face.v1, transform_matrix);
                var v2 = _project(face.v2, transform_matrix);
                var v3 = _project(face.v3, transform_matrix);

                this.drawPoint(v1, c_white);
                this.drawPoint(v2, c_white);
                this.drawPoint(v3, c_white);

                this.drawBresenhamLine(v1, v2, c_white);
                this.drawBresenhamLine(v2, v3, c_white);
                this.drawBresenhamLine(v1, v3, c_white);
            }, this);
        };

        return Shader;
    })();

    srender.Camera = (function () {
        function Camera() {
            this.pos = srender.Vec3.Zero();
            this.target = srender.Vec3.Zero();
        }
        return Camera;
    })();

    srender.Mesh = (function () {
        function Mesh(vertNum, faceNum) {
            this.vertices = new Array(vertNum);
            this.faces = new Array(faceNum);
            this.rot = srender.Vec3.Zero();
            this.pos = srender.Vec3.Zero();
        }
        return Mesh;
    })();

    srender.Face = (function () {
        function Face(v1, v2, v3) {
            this.v1 = v1;
            this.v2 = v2;
            this.v3 = v3;
        }
        return Face;
    })();

    return srender;
}(srender || {}));
